import rclpy
from threading import Thread
from rclpy.executors import MultiThreadedExecutor

from isae_robot_skillset_client import *
from isae_robot_skillset_interfaces.msg import *
from geometry_msgs.msg import Pose2D
from std_msgs.msg import Float64, String

# ROS2 init
rclpy.init()

# Create Client Instance
isae_robot = IsaeRobotSkillsetClient(node_name="isae_robot_mission", 
                                     skillset_manager="aias_lab_skillset")

# Spin in separate thread
executor = MultiThreadedExecutor()
executor.add_node(isae_robot.node)
executor_thread = Thread(target=executor.spin)
executor_thread.start()

