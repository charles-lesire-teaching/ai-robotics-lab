
type {
    Pose
    Float
    Color
}

skillset isae_robot {

    data pose: Pose

    resource {
        motion_status {
            state { Idle Busy }
            initial Idle
            transition all
        }
        battery_status {
            state { BatteryOk BatteryLow }
            initial BatteryOk
            transition {
                BatteryOk -> BatteryLow
            }
        }
    }

    event {
        estop {
            effect motion_status -> Idle
        }
        battery_to_low {
            guard battery_status == BatteryOk
            effect battery_status -> BatteryLow
        }
    }

    skill detect_target {
        input {
            timeout: Float
        }
        output {
            target: Color
        }
        interrupt { interrupting false }
        success target_found {}
        failure no_target {}
    }

    skill move_to {
        input {
            target: Pose
        }
        precondition {
            is_idle: motion_status == Idle
        }
        start motion_status -> Busy

        invariant {
            is_busy {
                guard motion_status == Busy
            }
        }

        progress {
            period 1.0
            output distance: Float
        }
        interrupt {
            interrupting true
            effect motion_status -> Idle
        }
        success reached {
            effect motion_status -> Idle
        }
        failure couldnot_reach {
            effect motion_status -> Idle
        }
    }

}